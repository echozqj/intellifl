/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class Classes
{
	public static String toDotClassName(String name){
		// 3X faster than JDK String.replace
		return StringUtils.replace(name, "/", ".");
	}
	public static String toSlashClassName(String name){
		return StringUtils.replace(name, ".", "/"); 
		//return name.replace(".", "/");
	}
	
	public static String descToClassName(String str){
		if(str.endsWith(";")){
			int i=str.indexOf("L");
			str=str.substring(i+1, str.length()-1);
		}
		return str;
	}
	
	public static boolean isPrimitive(String str){
		return !str.endsWith(";");
	}
	
	public static List<String> traverseClassDir(String dir) throws IOException, Exception {
		List<String> classFiles=new ArrayList<String>();
		File dirFile = new File(dir);
		List<File> workList = new ArrayList<File>();
		workList.add(dirFile);
		while (!workList.isEmpty()) {
			File curF = workList.remove(0);
			if (curF.getName().endsWith(".class")) {
				classFiles.add(curF.getAbsolutePath());
			} else if (curF.isDirectory()) {
				for (File f : curF.listFiles())
					workList.add(f);
			}
		}
		return classFiles;
	}
}
