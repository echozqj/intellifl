/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import set.intelliFL.log.IntelliFLLogger;

public class Properties
{

	public static final String GZ_EXT = ".gz";
	public static final String IFL_DIR = "intelliFL";

	public static final String STMT_COV = "stmt-cov";
	public static final String METH_COV = "meth-cov";
	public static String COV_TYPE_KEY = "covLevel=";
	public static String COV_TYPE = METH_COV;

	public static final String CONFIG = ".config";

	public static final String TIMING_DIR = IFL_DIR + "-" + "timing";
	public static String IFL_COV_DIR = IFL_DIR + "-" + METH_COV;
	public static final String TIMING_FILE = "timing.txt";
	public static String IFL_STMTCOV_DIR = IFL_DIR + "-" + STMT_COV;
	public static final String IFL_CG_DIR = IFL_DIR + "-cg";
	public static final String IFL_METRIC_DIR = IFL_DIR + "-metrics";
	public static final String IFL_OUTPUT_DIR = IFL_DIR + "-output";

	public static final String IFL_pythonSouce = IFL_DIR + "-PRFLPython";

	public static String AGENT_JAR_KEY = "IFL_Jar=";
	public static String AGENT_JAR;

	public static String AGENT_ARG;

	public static String BASE_DIR_KEY = "baseDir=";
	public static String BASE_DIR = System.getProperty("user.dir");

	public static String CLS_DIR_KEY = "clsDir=";
	public static String CLS_DIR = System.getProperty("user.dir");

	public static String TIMING_KEY = "timing=";
	public static boolean TIMING = false;

	public static String TIMING_PATH = BASE_DIR + File.separator + IFL_DIR
			+ File.separator + TIMING_DIR;

	public static void preprocess(String arguments) {
		if (arguments == null)
			return;
		AGENT_ARG = arguments;
		String[] items = arguments.split(",");
		for (String item : items) {
			if (item.startsWith(AGENT_JAR_KEY)) {
				AGENT_JAR = item.replace(AGENT_JAR_KEY, "");
			}
			// has to disable the parameter passing for now since the right one
			// did not get through
			/*
			 * else if (item.startsWith(CLS_DIR_KEY)) { CLS_DIR =
			 * item.replace(CLS_DIR_KEY, ""); } else if
			 * (item.startsWith(BASE_DIR_KEY)) { BASE_DIR =
			 * item.replace(BASE_DIR_KEY, ""); }
			 */ else if (item.startsWith(COV_TYPE_KEY)) {
				COV_TYPE = item.replace(COV_TYPE_KEY, "");
				IFL_COV_DIR = IFL_DIR + "-" + COV_TYPE;
			} else if (item.startsWith(TIMING_KEY))
				TIMING = Boolean.parseBoolean(item.replace(TIMING_KEY, ""));
		}
		IntelliFLLogger.debug(" AGENT_ARG: " + AGENT_ARG);
		IntelliFLLogger.debug(" BASE_DIR: " + BASE_DIR);
		IntelliFLLogger.debug(" CLS_DIR: " + CLS_DIR);
	}

	public static void serializeConfig(String string, String base) {
		String configPath = base+ File.separator
				+ Properties.IFL_DIR + File.separator + Properties.CONFIG;
		IntelliFLLogger.debug(" serializeConfig: " + configPath);
		File config = new File(configPath);
		config.getParentFile().mkdirs();

		try {
			PrintWriter writer = new PrintWriter(config);
			writer.println(string);
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static String deserializeConfig() {
		File config = new File(Properties.BASE_DIR + File.separator
				+ Properties.IFL_DIR + File.separator + Properties.CONFIG);

		try {
			BufferedReader reader = new BufferedReader(new FileReader(config));
			String content = reader.readLine();
			reader.close();
			return content;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
