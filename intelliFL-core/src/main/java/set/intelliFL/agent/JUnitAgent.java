/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL.agent;

import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;


import set.intelliFL.Properties;
import set.intelliFL.junit.JUnitTestMethodLevelTransformer;
import set.intelliFL.log.IntelliFLLogger;
import set.intelliFL.maven.SurefireTransformer;


public class JUnitAgent
{

	/**
	 * Premain for starting with surefire/failsafe test execution to capture dynamic
	 * dependency information
	 * 
	 * @param args
	 * @param inst
	 * @throws Exception
	 */
	public static void premain(String args, Instrumentation inst)
			throws Exception {
		IntelliFLLogger.info("premain");
		Properties.CLS_DIR=Properties.deserializeConfig();
		Properties.preprocess(args);
        
		if(!Properties.TIMING)
			inst.addTransformer(new ClassTransformer());
		inst.addTransformer(new JUnitTestMethodLevelTransformer());
	}

	/**
	 * Agentmain for capturing surefire/failsafe events to capture junit events
	 * 
	 * @param args
	 * @param inst
	 */
	public static void agentmain(String args, Instrumentation inst) {
		IntelliFLLogger.info("agentmain");
        Properties.preprocess(args);
		inst.addTransformer(new SurefireTransformer(), true);
		try {
			for (Class localClass : inst.getAllLoadedClasses()) {
				String str = localClass.getName();
				if ((str.equals(
						"org.apache.maven.plugin.surefire.AbstractSurefireMojo"))
						|| (str.equals(
								"org/apache/maven/plugin/surefire/SurefirePlugin"))
						|| (str.equals(
								"org/apache/maven/plugin/failsafe/IntegrationTestMojo"))
						|| (str.equals("org/scalatest/tools/maven/TestMojo"))) {
					inst.retransformClasses(localClass);
				}
			}
		} catch (UnmodifiableClassException localUnmodifiableClassException) {
		}

	}
	


}
