/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL.cg;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.objectweb.asm.ClassReader;

import set.intelliFL.Classes;
import set.intelliFL.Properties;
import set.intelliFL.log.IntelliFLLogger;

public class CallGraphBuilder
{
	public static final String CG_FILE = "CHA.cg";
	
	/**
	 * Main cg function, take a class directory or jar file as input, and analyze
	 * all class files under it
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		String path = args[0];
		if (path.endsWith(".jar"))
			build(new JarFile(path));
		else
			build(path);
	}

	public static void build(List<String> classFiles) {
		try {
			String path = getCGPath();

			PrintStream printer;

			printer = new PrintStream(new FileOutputStream(path));

			long time1 = System.currentTimeMillis();
			CallGraphBuilder.analyzeInheritance(classFiles);
			CallGraphBuilder.analyzeCG(classFiles);
			long time2 = System.currentTimeMillis();
			CallGraph.printCG(printer);
			CallGraph.printStatistics();
			IntelliFLLogger.info("CG construction cost: " + (time2 - time1) + " ms");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void build(String dir) throws Exception {
		List<String> files = Classes.traverseClassDir(dir);
		build(files);
	}

	public static void build(JarFile jar) throws IOException {
		String path =getCGPath();
		PrintStream printer = new PrintStream(new FileOutputStream(path));
		long time1 = System.currentTimeMillis();
		CallGraphBuilder.analyzeInheritance(jar);
		CallGraphBuilder.analyzeCG(jar);
		long time2 = System.currentTimeMillis();
		CallGraph.printCG(printer);
		printer = System.out;
		CallGraph.printStatistics();
		IntelliFLLogger.info("CG construction cost: " + (time2 - time1) + "ms");
	}

	private static String getCGPath() {
		String path = Properties.BASE_DIR + File.separator + Properties.IFL_DIR
				+ File.separator + Properties.IFL_CG_DIR;
		File file = new File(path);
		file.mkdirs();
		path = path + File.separator + CG_FILE;
		return path;
	}

	public static void findReachableClasses(String[] spots) throws IOException {
		int SpotNum = 0;
		for (String spot : spots) {
			String[] items = spot.split(":");
			String clazz = items[0];
			String meth = items[1];
			String id = Data.classNameMap.get(clazz) + ":"
					+ Data.methodNameMap.get(meth);
			Set<String> reachable = CallGraph.getTransitiveCallee(id);
			Set<String> reachableClasses = new HashSet<String>();
			for (String m : reachable) {
				String c = m.substring(0, m.indexOf(":"));
				reachableClasses.add(c);
			}

			BufferedWriter writer = new BufferedWriter(
					new FileWriter((SpotNum++) + "-log-class.txt"));
			for (String c : reachableClasses) {
				writer.write(
						Data.classNameMap.inverse().get(Integer.parseInt(c))
								+ "\n");
			}
			writer.flush();
			writer.close();
			System.out.println("Reachable classes: " + spot + "==>"
					+ reachableClasses.size());
		}
	}

	public static void findReachableMethods(String[] spots) throws IOException {
		int SpotNum = 0;
		for (String spot : spots) {
			String[] items = spot.split(":");
			String clazz = items[0];
			String meth = items[1];
			String id = Data.classNameMap.get(clazz) + ":"
					+ Data.methodNameMap.get(meth);
			Set<String> reachableMethods = CallGraph.getTransitiveCallee(id);

			BufferedWriter writer = new BufferedWriter(
					new FileWriter((SpotNum++) + "-log-meth.txt"));
			for (String m : reachableMethods) {
				String[] contents = m.split(":");
				writer.write(Data.classNameMap.inverse()
						.get(Integer.parseInt(contents[0])) + ":"
						+ Data.methodNameMap.inverse()
								.get(Integer.parseInt(contents[1]))
						+ "\n");
				writer.write(m + "\n");// debugging
			}
			writer.flush();
			writer.close();
			System.out.println("Reachable methods: " + spot + "==>"
					+ reachableMethods.size());
		}
	}

	public static void analyzeInheritance(JarFile f) throws IOException {
		Enumeration<JarEntry> entries = f.entries();
		while (entries.hasMoreElements()) {
			JarEntry entry = entries.nextElement();

			String entryName = entry.getName();
			if (entryName.endsWith(".class")) {
				InputStream classFileInputStream = f.getInputStream(entry);
				ClassReader cr = new ClassReader(classFileInputStream);
				ClassInheritanceVisitor ca = new ClassInheritanceVisitor();
				cr.accept(ca, 0);
				classFileInputStream.close();
			}
		}
	}

	public static void analyzeInheritance(List<String> classFiles)
			throws IOException {
		for (String f : classFiles) {
			InputStream classFileInputStream = new FileInputStream(f);
			ClassReader cr = new ClassReader(classFileInputStream);
			ClassInheritanceVisitor ca = new ClassInheritanceVisitor();
			cr.accept(ca, 0);
			classFileInputStream.close();
		}
	}

	public static void analyzeCG(JarFile f) throws IOException {
		Enumeration<JarEntry> entries = f.entries();
		while (entries.hasMoreElements()) {
			JarEntry entry = entries.nextElement();

			String entryName = entry.getName();
			if (entryName.endsWith(".class")) {
				InputStream classFileInputStream = f.getInputStream(entry);
				ClassReader cr = new ClassReader(classFileInputStream);
				ClassCGVisitor ca = new ClassCGVisitor();
				cr.accept(ca, 0);
				classFileInputStream.close();
			}
		}
	}

	public static void analyzeCG(List<String> classFiles) throws IOException {
		for (String f : classFiles) {
			InputStream classFileInputStream = new FileInputStream(f);
			ClassReader cr = new ClassReader(classFileInputStream);
			ClassCGVisitor ca = new ClassCGVisitor();
			cr.accept(ca, 0);
			classFileInputStream.close();
		}
	}

}
