/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL.cg;

import java.util.HashMap;
import java.util.HashSet;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

class ClassInheritanceVisitor extends ClassVisitor implements Opcodes
{
	String cName;

	public ClassInheritanceVisitor() {
		super(ASM5);
	}

	public void visit(int version, int access, String name, String signature,
			String superName, String[] interfaces) {
		cName = name;
		// record inheritance relationship
		for (String interf : interfaces) {
			if (!Data.inheritanceMap.containsKey(interf))
				Data.inheritanceMap.put(interf, new HashSet<String>());
			Data.inheritanceMap.get(interf).add(name);
		}
		if (!Data.inheritanceMap.containsKey(superName))
			Data.inheritanceMap.put(superName, new HashSet<String>());
		Data.inheritanceMap.get(superName).add(name);

		// record inverse inheritance relationship
		if (!Data.inverseInheritanceMap.containsKey(name))
			Data.inverseInheritanceMap.put(name, new HashSet<String>());
		Data.inverseInheritanceMap.get(name).add(superName);
		for (String interf : interfaces)
			Data.inverseInheritanceMap.get(name).add(interf);

		// encode the class name
		if (!Data.classNameMap.containsKey(name))
			Data.classNameMap.put(name, Data.classNameMap.size());
		Data.classMethodMap.put(Data.classNameMap.get(name),
				new HashSet<Integer>());
	}

	public MethodVisitor visitMethod(int access, String name, String desc,
			String signature, String[] exceptions) {
		String meth = name + desc;

		// encode method name
		if (!Data.methodNameMap.containsKey(meth))
			Data.methodNameMap.put(meth, Data.methodNameMap.size());
		Data.classMethodMap.get(Data.classNameMap.get(cName))
				.add(Data.methodNameMap.get(meth));
		return null;
	}
}
