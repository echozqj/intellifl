/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL.cg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

public class Data
{
	static Map<String, Set<String>> inheritanceMap = new HashMap<String, Set<String>>();
	static Map<String, Set<String>> inverseInheritanceMap = new HashMap<String, Set<String>>();
	static BiMap<String, Integer> classNameMap = HashBiMap.create();
	static BiMap<String, Integer> methodNameMap = HashBiMap.create();
	static Map<Integer, Set<Integer>> classMethodMap = new HashMap<Integer, Set<Integer>>();

	public static void clear() {
		inheritanceMap.clear();
	}

	public static String getEncoding(String name) {
		String[] items = name.split(":");
		return Data.classNameMap.get(items[0]) + ":"
				+ Data.methodNameMap.get(items[1]);
	}

	public static String getDecoding(String name) {
		String[] items = name.split(":");
		return Data.classNameMap.inverse().get(Integer.parseInt(items[0])) + ":"
				+ Data.methodNameMap.inverse().get(Integer.parseInt(items[1]));
	}

	public static Set<String> getTransitiveSubclasses(String clazz) {
		Set<String> subclasses = new HashSet<String>();
		List<String> worklist = new ArrayList<String>();
		worklist.add(clazz);
		while (!worklist.isEmpty()) {
			String cur = worklist.remove(0);
			subclasses.add(cur);
			if (inheritanceMap.containsKey(cur)) {
				for (String curchild : inheritanceMap.get(cur)) {
					worklist.add(curchild);
				}
			}
		}
		return subclasses;
	}

	public static String getFirstSuperclassWithMethod(int clazzId,
			int methodId) {
		List<String> worklist = new ArrayList<String>();
		String clazz = Data.classNameMap.inverse().get(clazzId);
		worklist.add(clazz);
		while (!worklist.isEmpty()) {
			String cur = worklist.remove(0);
			Integer classId = Data.classNameMap.get(cur);
			if (classMethodMap.containsKey(classId)
					&& classMethodMap.get(classId).contains(methodId)) {
				return classId + ":" + methodId;
			}
			if (inverseInheritanceMap.containsKey(cur)) {
				for (String curchild : inverseInheritanceMap.get(cur)) {
					worklist.add(curchild);
				}
			}
		}
		return null;
	}
}
