/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL.instrument;

import java.util.BitSet;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import set.intelliFL.Properties;
import set.intelliFL.log.IntelliFLLogger;

public class ClassTransformVisitor extends ClassVisitor implements Opcodes
{
	int clazzId;
	String slashClazzName;
	String dotClazzName;

	public ClassTransformVisitor(int clazzId, String clazzName,
			String dotClassName, final ClassVisitor cv) {
		super(ASM5, cv);
		this.clazzId = clazzId;
		this.slashClazzName = clazzName;
		this.dotClazzName = dotClassName;
		CoverageData.registerMeth(clazzId, CoverageData.CLINIT);
	}

	@Override
	public MethodVisitor visitMethod(final int access, final String name,
			final String desc, final String signature,
			final String[] exceptions) {
		MethodVisitor mv = cv.visitMethod(access, name, desc, signature,
				exceptions);
		if(!MethodFilter.shouldInstrument(access)){
			IntelliFLLogger.debug("Skip instrumenting: "+dotClazzName+":"+name+":"+desc);
			return mv;
		}
		
		String methName = name + ":" + desc;
		
		int methId = CoverageData.registerMeth(clazzId, methName);
		boolean isInit = name.startsWith("<init>");

		if (Properties.COV_TYPE.endsWith(Properties.STMT_COV))
			return mv == null ? null
					: new MethodVisitorStmtCov(mv, clazzId, methId, isInit,
							access);
		if (Properties.COV_TYPE.endsWith(Properties.METH_COV))
			return mv == null ? null
					: new MethodVisitorMethCov(mv, clazzId, methId,
							slashClazzName, dotClazzName, methName, isInit,
							access);
		return mv;
	}

	@Override
	public void visitEnd() {
		super.visitEnd();
		if (CoverageData.idMethMap.containsKey(clazzId)) {
			if (Properties.COV_TYPE.endsWith(Properties.METH_COV)) {
				CoverageData.methCovArray[clazzId] = new boolean[CoverageData.idMethMap
						.get(clazzId).size()];
			} else if (Properties.COV_TYPE.endsWith(Properties.STMT_COV)) {
				CoverageData.stmtCovSet[clazzId] = new BitSet[CoverageData.idMethMap
						.get(clazzId).size()];
				for (int m = 0; m < CoverageData.stmtCovSet[clazzId].length; m++) {
					CoverageData.stmtCovSet[clazzId][m] = new BitSet();
				}
			}
		}

	}

}
