/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL.instrument;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

import set.intelliFL.instrument.CoverageData;
public class Tracer
{
	public final static String TRACE_CLASS_COV = "traceClassCovInfo";
	public final static String TRACE_METH_COV = "traceMethCovInfo";
	public final static String TRACE_METH_COV_RT = "traceMethCovInfoWithRT";
	public final static String TRACE_CLINIT = "traceMethCovInfoClinit";
	public final static String TRACE_STMT_COV = "traceStmtCovInfo";
	public final static String TRACE_BLOCK_COV = "traceBlockCovInfo";
	public final static String TRACE_BRANCH_COV = "traceBranchCovInfo";

	// trace method coverage
	public static void traceMethCovInfo(int clazzId, int methId) {
		CoverageData.methCovArray[clazzId][methId] = true;
	}
	public static void traceMethCovInfoClinit(int clazzId) {
		CoverageData.methCovArray[clazzId][0] = true;
	}

	public static void traceMethCovInfoClinit(String clazz) {
		Integer clazzId = CoverageData.classIdMap.get(clazz);
		if (clazzId != null)
			traceMethCovInfoClinit(clazzId);
	}

	public static void traceMethCovInfoClinit(Class<?> clazz) {
		String name = clazz.getName();
		traceMethCovInfoClinit(name);
	}

	public static void traceMethCovInfoClinit(Object clazz) {
		if (clazz != null) {
			traceMethCovInfoClinit(clazz.getClass().getName());
		}
	}
	
	// trace statement coverage
	public static void traceStmtCovInfo(int clazzId, int methId, int line) {
		CoverageData.stmtCovSet[clazzId][methId].set(line);
	}
}
