/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


import set.intelliFL.Properties;
import set.intelliFL.instrument.CoverageData;

public class TracerIO
{
	public final static String CLASS = "set/faulttracer/coverage/io/TracerIO";
	
	
	public static void writeMethodCov(String testName, boolean outcome,
			String path) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(path));
		writer.write(testName + " " + outcome + "\n");
		int classNum = CoverageData.classId;
		for (int clazzId = 0; clazzId < classNum; clazzId++) {
			boolean[] meths = CoverageData.methCovArray[clazzId];
			if (meths == null)
				continue;
			int methNum = meths.length;
			for (int i = 0; i < methNum; i++) {
				if (meths[i]) {
					// long id = clazzId << 32 + i;
					// writer.write(id + " ");
					writer.write(CoverageData.idClassMap.get(clazzId) + ":"
							+ CoverageData.idMethMap.get(clazzId).get(i)
							+ "\n");
					meths[i] = false;
				}
			}
		}
		writer.flush();
		writer.close();
	}

	public static void writeStmtCov(String testName, boolean outcome,
			String path) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(path));
		writer.write(testName + " " + outcome + "\n");
		int classNum = CoverageData.classId;
		for (int clazzId = 0; clazzId < classNum; clazzId++) {
			BitSet[] meths = CoverageData.stmtCovSet[clazzId];
			if (meths == null)
				continue;
			for (int m = 0; m < meths.length; m++) {
				BitSet stmts = CoverageData.stmtCovSet[clazzId][m];
				if (stmts == null)
					continue;
				int stmtNum = stmts.size();
				for (int s = 1; s < stmtNum; s++) {
					if (stmts.get(s)) {
						writer.write(CoverageData.idClassMap.get(clazzId) + ":"
								+ CoverageData.idMethMap.get(clazzId).get(m)
								+ ":" + s + "\n");
						stmts.clear(s);
					}
				}
			}
		}
		writer.flush();
		writer.close();
	}

	public static Map<String, Set<String>> loadCovFromDirectory()
			throws IOException {
		File dir = new File(Properties.BASE_DIR
				+ File.separator + Properties.IFL_COV_DIR);
		if (!dir.exists()) {
			return null;
		}
		File[] tests = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(Properties.GZ_EXT);
			}
		});
		Map<String, Set<String>> result = new HashMap<String, Set<String>>();
		for (File f : tests) {
			loadCoverage(f, result);
		}
		return result;
	}

	public static void loadCoverage(File file, Map<String, Set<String>> result)
			throws IOException {
		Set<String> coveredElements = new HashSet<String>();
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String testName = reader.readLine();
		String line = reader.readLine();
		while (line != null) {
			coveredElements.add(line);
			line = reader.readLine();
		}
		reader.close();
		result.put(testName, coveredElements);
	}
}
