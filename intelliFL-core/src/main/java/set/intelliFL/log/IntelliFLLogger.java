/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL.log;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class IntelliFLLogger
{
	static boolean DEBUG = false;
	static boolean LOG = true;
	static boolean WARN = true;
	static boolean ERR = true;

	public static void log(String s, String path) {
		try {
			System.out.println("+++" + s);
			BufferedWriter writer = new BufferedWriter(
					new FileWriter(path, true));
			writer.write(s + "\n");
			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void debug(String s) {
		if (DEBUG)
			System.out.println("[IntelliFL DEBUG] " + s);
	}

	public static void info(String s) {
		if (LOG)
			System.out.println("[IntelliFL INFO] " + s);
	}

	public static void warn(String s) {
		if (WARN)
			System.out.println("[IntelliFL WARN] " + s);
	}

	public static void err(String s) {
		if (ERR)
			System.out.println("[IntelliFL ERROR] " + s);
	}

}
