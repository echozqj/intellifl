/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL.metrics;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.ClassNode;

import set.intelliFL.Classes;
import set.intelliFL.Properties;

public class MetricAnalyzer
{
	public static Map<String, MetricData> metrics = new HashMap<String, MetricData>();
	public static int classCount = 0;
	public static final String METRIC_FILE = "code.metrics";

	/**
	 * Main function, take a class directory or jar file as input, and analyze
	 * all class files under it
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		long start_time = System.currentTimeMillis();
		String path = args[0];
		if (path.endsWith(".jar"))
			analyze(new JarFile(path));
		else
			analyze(path);
	}

	public static void analyze(String dir) throws IOException, Exception {
		List<String> files = Classes.traverseClassDir(dir);
		analyze(files);
	}
	public static void analyze(JarFile jarFile) {
		long start_time = System.currentTimeMillis();
		metrics.clear();
		classCount = 0;

		String outPath = getMetricPath();
		try {
			analyzeMetric(jarFile);
			serialize(outPath);
		} catch (Exception e) {
			e.printStackTrace();
		}

		long end_time = System.currentTimeMillis();
		PrintStream printer = System.out;
		printer.println("MetricAnalyzer analyzed " + classCount
				+ " classes using: " + (end_time - start_time) + " ms");
	}
	
	public static void analyze(List<String> classFiles) {
		long start_time = System.currentTimeMillis();
		metrics.clear();
		classCount = 0;

		String outPath = getMetricPath();
		try {
			analyzeMetric(classFiles);
			serialize(outPath);
		} catch (Exception e) {
			e.printStackTrace();
		}

		long end_time = System.currentTimeMillis();
		PrintStream printer = System.out;
		printer.println("MetricAnalyzer analyzed " + classCount
				+ " classes using: " + (end_time - start_time) + " ms");
	}

	private static String getMetricPath() {
		String path = Properties.BASE_DIR + File.separator + Properties.IFL_DIR
				+ File.separator + Properties.IFL_METRIC_DIR;
		File file = new File(path);
		file.mkdirs();
		path = path + File.separator + METRIC_FILE;
		return path;
	}

	public static void serialize(String path) throws IOException {
		PrintStream printer = new PrintStream(path);
		for (String m : metrics.keySet()) {
			printer.println(m + " " + metrics.get(m).toString());
		}
		printer.close();
	}


	public static void analyzeMetric(List<String> classFiles)
			throws IOException, Exception {
		for (String file : classFiles) {
			InputStream classFileInputStream = new FileInputStream(file);
			ClassReader cr = new ClassReader(classFileInputStream);
			ClassMetricVisitor ca = new ClassMetricVisitor();
			cr.accept(ca, 0);
			classFileInputStream.close();
		}
	}

	public static void analyzeMetric(JarFile f) throws IOException {
		Enumeration<JarEntry> entries = f.entries();
		while (entries.hasMoreElements()) {
			JarEntry entry = entries.nextElement();

			String entryName = entry.getName();
			if (entryName.endsWith(".class")) {
				InputStream classFileInputStream = f.getInputStream(entry);
				ClassReader cr = new ClassReader(classFileInputStream);
				ClassMetricVisitor ca = new ClassMetricVisitor();
				cr.accept(ca, 0);
				classFileInputStream.close();
			}
		}
	}

	private static ClassNode parseClassNode(final String classFileName)
			throws IOException, FileNotFoundException {
		final ClassReader cr = new ClassReader(
				new FileInputStream(classFileName));
		// create an empty ClassNode (in-memory representation of a class)
		final ClassNode clazz = new ClassNode();
		// have the ClassReader read the class file and populate the ClassNode
		// with the corresponding information
		cr.accept(clazz, 0);
		return clazz;
	}

}
