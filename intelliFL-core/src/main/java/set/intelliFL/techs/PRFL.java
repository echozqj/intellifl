package set.intelliFL.techs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import set.intelliFL.Properties;
import set.intelliFL.log.IntelliFLLogger;

public class PRFL {
	public final static String AGG="aggregation";
	public final static String SPEC_TECH="specTech";
	public static void main(String[] args){
		boolean aggregation=false;
		String specTech="PR_Ochiai";
		if(System.getProperty(AGG)!=null)
			aggregation=Boolean.parseBoolean(System.getProperty(AGG));
		if(System.getProperty(SPEC_TECH)!=null)
			specTech=System.getProperty(SPEC_TECH);
		
		String prflPyPath=Properties.IFL_DIR+File.separator+Properties.IFL_pythonSouce;
		PRFL.makePyDir(prflPyPath);
		//Read and store python script 
		PRFL.storePyScripts(prflPyPath);
		//run prfl python code
		PRFL.runPrfl(prflPyPath,aggregation,specTech);
	}
	private static String readFromInputStream(InputStream inputStream) throws IOException {
		StringBuilder resultStringBuilder = new StringBuilder();
		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
		String line;
		while ((line = br.readLine()) != null) {
			resultStringBuilder.append(line).append("\n");
		}
		return resultStringBuilder.toString();
	}
	public static void makePyDir(String prflPyPath){
		File prflpyDir = new File(prflPyPath);
		if (!prflpyDir.exists()){
			try{
				prflpyDir.mkdirs();
			} 
			catch(SecurityException se){}        
		}
		
	}
	public static void storePyScripts(String prflPyPath){
		ArrayList<String> allPyFiles=new ArrayList<String>();
		File jarFile = new File(Properties.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		JarFile jar=null;
		try {
			jar = new JarFile(jarFile);
			final Enumeration<JarEntry> entries = jar.entries(); //gives ALL entries in jar
		    while(entries.hasMoreElements()) {
		        final String name = entries.nextElement().getName();
		        
		        if (name.contains(".py")) { //filter according to the path
		        	allPyFiles.add(name);
		        }
		    }
		    jar.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		for(String pyFile:allPyFiles){
			InputStream input = Properties.class.getResourceAsStream("/"+pyFile);
			BufferedWriter writer;
			try {
				String pyName=pyFile.substring(pyFile.lastIndexOf("/")+1);
				writer = new BufferedWriter(new 
						 			FileWriter(prflPyPath+File.separator+pyName));
				writer.write(readFromInputStream(input));
				writer.flush();
				writer.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
	}
	public static void printProcessInfo(Process pr){
		try{
			BufferedReader in = new BufferedReader(new  InputStreamReader(pr.getInputStream()));  
	        String line;  
	        while ((line = in.readLine()) != null) {  
	            System.out.println(line);
	            if(line.contains("no failed tests found")){
	            	IntelliFLLogger.info("All tests pass, IntelliFL will not apply");
	            	System.exit(0);
	            }
	        }  
	        BufferedReader stdError = new BufferedReader(new 
	        	     InputStreamReader(pr.getErrorStream()));
	        String s = null;
	        while ((s = stdError.readLine()) != null) {
	            System.out.println(s);
	        }
	        in.close();  
	        pr.waitFor();
		}catch (Exception e){  
			e.printStackTrace();  
		} 
	}
	public static void runPrfl(String prflPyPath,boolean aggregation,String specTech){
		String coveragePath=Properties.IFL_DIR+File.separator+Properties.IFL_COV_DIR;
		String statCovPath=Properties.IFL_DIR+File.separator+Properties.IFL_STMTCOV_DIR;
		String callgraphPath=Properties.IFL_DIR+File.separator+Properties.IFL_CG_DIR;
		String output=Properties.IFL_DIR+File.separator+Properties.IFL_OUTPUT_DIR;
		File outputDir = new File(output);
		if (!outputDir.exists()){
			try{
				outputDir.mkdirs();
			} 
			catch(SecurityException se){}        
		}
		
		String damping_factor="0.7";
		String alpha="0.001";
		String delta="1.0";
		
		String cmd1="python "+prflPyPath+File.separator+"runPageRank.py "
					+callgraphPath+" "+coveragePath+" "+outputDir+" "
				    +damping_factor+" "+alpha+" "+delta+" "+statCovPath
				    +" "+aggregation;
		String cmd2="python "+prflPyPath+File.separator+"rankMethods.py "+outputDir+" "+outputDir+" "+
					specTech;
		
		long time1 = System.currentTimeMillis();
		try{  
            Process pr = Runtime.getRuntime().exec(cmd1);  
            printProcessInfo(pr);
            
            Process pr2=Runtime.getRuntime().exec(cmd2); 
            printProcessInfo(pr2);
            
             
		} catch (Exception e){  
			e.printStackTrace();  
		} 
		long time2 = System.currentTimeMillis();
		IntelliFLLogger.info("PageRank fault localization DONE");
		IntelliFLLogger.info("PageRank fault localization implementation cost: " + (time2 - time1) + " ms");
		
	}

}
