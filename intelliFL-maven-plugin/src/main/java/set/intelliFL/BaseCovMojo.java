/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL;

import java.io.File;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Plugin;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.twdata.maven.mojoexecutor.MojoExecutor;

import set.intelliFL.log.IntelliFLLogger;
import set.intelliFL.maven.DynamicAgentLoader;

/**
 * This mojo is the parent class of MethCovMojo.java and StmtCovMojo.java
 * 
 * @author lingmingzhang
 *
 */
@Mojo(requiresDependencyResolution = ResolutionScope.TEST, name = "")
public class BaseCovMojo extends BaseMojo
{
	/**
	 * This is method attaches java agent to surefire or failsafe runs
	 */
	public void execute() throws MojoExecutionException {

		agentJar = getPathToIntelliFLJar();
		String arguments = getArguments();

		DynamicAgentLoader.loadDynamicAgent(agentJar, arguments);

		runSurefireTests();

		if (!skipIT)
			runIntegrationTests();
	}

	/**
	 * This invokes surefire run programmatically
	 * 
	 * @throws MojoExecutionException
	 */
	private void runSurefireTests() throws MojoExecutionException {
		this.surefire = this
				.lookupPlugin("org.apache.maven.plugins:maven-surefire-plugin");
		if (this.surefire == null) {
			getLog().error("Make sure surefire is in your pom.xml!");
		}
		Xpp3Dom surefireDomNode = (Xpp3Dom) this.surefire.getConfiguration();
		if (surefireDomNode == null)
			surefireDomNode = new Xpp3Dom("configuration");
		MojoExecutor.executeMojo(this.surefire, "test", surefireDomNode,
				MojoExecutor.executionEnvironment(this.mavenProject,
						this.mavenSession, this.pluginManager));
	}

	private void runIntegrationTests() throws MojoExecutionException {
		this.failsafe = this
				.lookupPlugin("org.apache.maven.plugins:maven-failsafe-plugin");

		if (this.failsafe != null) {
			IntelliFLLogger.info("Have integration test");
			Xpp3Dom failsafeDomNode = (Xpp3Dom) this.failsafe
					.getConfiguration();
			if (failsafeDomNode == null)
				failsafeDomNode = new Xpp3Dom("configuration");
			MojoExecutor.executeMojo(this.failsafe, "integration-test",
					failsafeDomNode,
					MojoExecutor.executionEnvironment(this.mavenProject,
							this.mavenSession, this.pluginManager));
		} else {
			IntelliFLLogger.info("No integration test");
		}
	}

}
